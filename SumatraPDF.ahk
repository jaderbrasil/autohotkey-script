; Vim style hotkeys for Sumatra PDF.
#IfWinActive ahk_exe SumatraPDF.exe ahk_class SUMATRA_PDF_FRAME

$k::
	ControlGetFocus, ctrl
	if (ctrl == "Edit1" or ctrl == "Edit2")
		Send k
	else
		Send {k 4}
	Return

$+j::
	ControlGetFocus, ctrl
	if (ctrl == "Edit1" or ctrl == "Edit2")
		Send +k
	else
		Send {NumpadSub}
	Return

$j::
	ControlGetFocus, ctrl
	if (ctrl == "Edit1" or ctrl == "Edit2")
		Send j
	else
		Send {j 4}
	Return


$+k::
	ControlGetFocus, ctrl
	if (ctrl == "Edit1" or ctrl == "Edit2")
		Send +j
	else
		Send {NumpadAdd}
	Return

$u::
	ControlGetFocus, ctrl
	if (ctrl == "Edit1" or ctrl == "Edit2")
		Send u
	else
		Send {k 22}
	Return

$d::
	ControlGetFocus, ctrl
	if (ctrl == "Edit1" or ctrl == "Edit2")
		Send d
	else
		Send {j 22}
	Return

$n::
	ControlGetFocus, ctrl
	if (ctrl == "Edit1" or ctrl == "Edit2")
		Send, n
	else
		Send, {F3}
	Return
$+n::
	ControlGetFocus, ctrl
	if (ctrl == "Edit1" or ctrl == "Edit2")
		Send, N
	else
		Send, +{F3}
	Return
$x::
	ControlGetFocus, ctrl
	if (ctrl == "Edit1" or ctrl == "Edit2")
		Send, x
	else
		Send, ^w
	Return
+g::Send, {End 2}
^g::Send, {Home}

#IfWinActive Go to page ahk_exe SumatraPDF.exe ahk_class #32770
	g::Send, {Escape}{Home}
#IfWinActive
